from kontakt.config_options import build_default_config, Option


def test_default_config():
    config_options = (
        Option(
            (
                "widgets",
                "fnorder",
                "size",
            ),
            "",
            12,
        ),
        Option(
            (
                "widgets",
                "transmogrifier",
                "intensity",
            ),
            "",
            42,
        ),
    )
    actual = build_default_config(config_options)

    expected = {"widgets": {"fnorder": {"size": 12}, "transmogrifier": {"intensity": 42}}}

    assert actual == expected
