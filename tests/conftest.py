import pytest

from helpers import EXTENSION_TYPES


@pytest.fixture(scope="session", params=list(EXTENSION_TYPES.items()))
def extension_type(request):
    "Provides `(extension-name, extension-type)` tuples."
    return request.param
