import kontakt.examples.extension


EXTENSION_NAMESPACE = "kontakt.extension"

EXTENSION_TYPES = {
    "green": kontakt.examples.extension.Green,
    "blue": kontakt.examples.extension.Blue,
    "red": kontakt.examples.extension.Red,
}

CONFIGURABLE_EXTENSION_NAMESPACE = "kontakt.configurable_extension"
